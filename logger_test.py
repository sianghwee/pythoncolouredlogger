from LoggingWrapper import log_wrapper

import threading
from time import sleep

@log_wrapper.wrap
def stateless_increment(value):
    x = 0
    print("Initial:", x)
    x += value
    print("Final:", x)
    return x

x = 0
@log_wrapper.wrap
def stateful_increment(value):
    global x
    print("Initial:", x)
    x += value
    print("Final:", x)
    return x

@log_wrapper.wrap
def slow_increment(value):
    def silly_increment(lst):
        sleep(5)
        lst[0] += value
        return None
    x = [0]
    print("Initial:", x[0])
    t = threading.Thread(target=silly_increment, args=(x,))
    t.start()
    t.join()
    print("Final:", x[0])
    return x

@log_wrapper.wrap
def divide(x, y):
    return x / y

x = 2
class Calculate:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def sum(self):
        return self.x + self.y

    def divide(self):
        a = 1
        return self.x / self.y

    @log_wrapper.wrap
    def complicate(self):
        sum = self.sum()
        self.y = 0
        divide = self.divide()
        return sum * divide

print("Test stateless increment [PASS]")
stateless_increment(5)

print("Test stateful increment [PASS]")
stateful_increment(5)

print("Test keyword arguments [PASS]")
stateless_increment(value=4)

print("Test wrong keyword arguments [FAIL]")
stateless_increment(name=4)

print("Test wrong type [FAIL]")
stateless_increment("abc")

print("Test multiple arguments [FAIL]")
stateless_increment(1, 2, 3, value=4)

print("Test slow increment [PASS]")
slow_increment(3)

print("Testing divide by zero [FAIL]")
divide(3, 0)

print("Testing class functons")
Calculate(3, 5).complicate()