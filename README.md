# PythonColouredLogger

## Description
A POC to test if it is possible to have coloured logging messages for better user experience and readability.

Tested to work on:
1. Windows 11 Terminal
2. Ubuntu 22.01 Desktop

To be tested on:
1. Ubuntu 22.01 Server

## Getting started

To try out the file, just clone the repository and run the python file `logger_test.py`. There are some test cases to try out the different functionality and colours.

To change the colours, refer to the following link for a list of colours:
https://learn.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences.


## Design Decisions
This idea was taken from https://stackoverflow.com/a/56944256. However, some changes were made to improve the code. Instead of always, creating a new `logging.Formatter` object for each line of the log, I decided that it makes more sense to have 5 different `logging.Formatter` for each of the debugging level and call the `format` method from each of them instead. This will reduce the time required for each logging operation as a new object need not be created.

There was also the option of just having a single `logging.Formatter` and changing the style of it for each of the debugging level. While this is the most resource unintensive and adheres to the parent/child design of Python, this method is too intrusive as it involves changing the private attributes of the parent class, `logging.Formatter`. This makes it extremely prone to failing should there be changes to the internal mechanism of the parent class.
