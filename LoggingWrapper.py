import logging, functools, sys
from ColouredFormatter import ColouredFormatter

fmt = "%(asctime)s.%(msecs)03d - %(levelname)s - %(message)s"
datefmt = "%y%m%d %H:%M:%S"

# all of this parameters should be retrieved from a config file
console = True
if console:
    console_level = logging.ERROR

file = False
if file:
    filename = "test.log"
    file_level = logging.DEBUG

class LoggingWrapper():
    def __init__(self, logger_name="Logger", fmt=None, datefmt=None, console=True, console_level=logging.ERROR, 
                    file=False, filename="test.log", file_level=logging.DEBUG):
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG)

        if console:
            console_logger = logging.StreamHandler()
            console_logger.setLevel(console_level)
            console_logger.setFormatter(ColouredFormatter(fmt=fmt, datefmt=datefmt))
            self.logger.addHandler(console_logger)

        if file:
            file_logger = logging.FileHandler(filename=filename)
            file_logger.setLevel(file_level)
            file_logger.setFormatter(ColouredFormatter(fmt=fmt, datefmt=datefmt, coloured=False))
            self.logger.addHandler(file_logger)

    def _get_signature(self, func):
        return f"{func.__module__}.{func.__name__}"

    def wrap(self, func):
        @functools.wraps(func)
        def _wrapper_(*args, **kwargs):
            func_signature = self._get_signature(func)
            self.logger.debug(f"Running {func_signature}")
            try:
                result = func(*args, **kwargs)
            except Exception as e:
                _, e, tb = sys.exc_info()
                while tb.tb_next:
                    tb = tb.tb_next
                frame = tb.tb_frame
                del func_signature, tb, _ # remove excess information
                stack = []
                while frame: 
                    # store only the revelant object
                    items = frame.f_locals.items() # freeze the stack variables
                    filename = frame.f_code.co_filename
                    methodname = frame.f_code.co_name
                    lineno = frame.f_lineno
                    stack.append((filename, methodname, lineno, items))
                    del items, filename, methodname, lineno # need to remove
                    if frame == sys._getframe(0):
                        break # terminate if at wrapper frame
                    frame = frame.f_back
                stack.reverse()
                self.logger.error(self.get_stack(e, stack))
            else:
                self.logger.debug(f"Completed {func_signature}")
                return result
        return _wrapper_

    def get_stack(self, e, stack, depth=3):
        output = []
        for fn, mn, ln, items in stack[-depth:]:
            output.append(f"{type(e).__name__} @ {fn}.{mn}#{ln}")
            for k, v in items:
                if k in ('e', 'frame', 'func', 'stack') or v == self:
                    continue
                try:
                    output.append(f"\t{k}: {v.__str__()}")
                    if hasattr(v, '__dict__'):
                        attr = v.__dict__
                        for a in attr:
                            try:
                                output.append(f"\t{k}.{a}: {attr[a].__str__()}")
                            except:
                                output.append(f"\t{k}.{a}: ???")
                except:
                    output.append(f"\t{k}: ???")
        return "\n".join(output)

log_wrapper = LoggingWrapper(fmt=fmt, datefmt=datefmt, console=console, console_level=console_level, file=False)