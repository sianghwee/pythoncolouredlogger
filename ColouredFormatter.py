import logging, sys, os

def has_colour_support():
    '''
    Test to see if terminal supports colour. Returns True if terminal supports colour, else False.
    Retrieved from:
    https://github.com/django/django/blob/main/django/core/management/color.py#L21
    '''
    def vt_codes_enabled_in_win_registry():
        try:
            import winreg
        except ImportError:
            return False
        else:
            try:
                reg_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, "Console")
                reg_key_value, _ = winreg.QueryValueEx(reg_key, "VirtualTerminalLevel")
            except FileNotFoundError:
                return False
            else:
                return reg_key_value == 1

    try:
        import colorama
        colorama.init()
    except (ImportError, OSError):
        HAS_COLORAMA = False
    else:
        HAS_COLORAMA = True

    isatty = hasattr(sys.stdout, "isatty") and sys.stdout.isatty()

    return isatty and (
        sys.platform != "win32"
        or HAS_COLORAMA
        or "ANSICON" in os.environ
        or
        # Windows Terminal supports VT codes.
        "WT_SESSION" in os.environ
        or
        # Microsoft Visual Studio Code's built-in terminal supports colors.
        os.environ.get("TERM_PROGRAM") == "vscode"
        or vt_codes_enabled_in_win_registry()
    )

# Intentional move to overwrite function to prevent it from being called again
has_colour_support = has_colour_support()

class ColouredFormatter(logging.Formatter):
    '''
    Retrieved from 
    https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
    '''

    # Colors for messages
    GREY = "\x1b[38;20m"
    BLUE = "\x1b[34;20m"
    CYAN = "\x1b[36;20m"
    YELLOW = "\x1b[33;20m"
    RED = "\x1b[31;20m"
    BOLD_RED = "\x1b[31;1m"
    RESET = "\x1b[0m"

    def __init__(self, fmt=None, datefmt=None, coloured=True):
        self.coloured = coloured and has_colour_support
        self.base_formatter = logging.Formatter(fmt, datefmt)
        if not coloured:
            self.formatters = {}
        else:
            self.formatters = {
                logging.DEBUG: self.base_formatter,
                logging.INFO: logging.Formatter(ColouredFormatter.CYAN + self.base_formatter._fmt + ColouredFormatter.RESET, datefmt),
                logging.WARNING: logging.Formatter(ColouredFormatter.YELLOW + self.base_formatter._fmt + ColouredFormatter.RESET, datefmt),
                logging.ERROR: logging.Formatter(ColouredFormatter.RED + self.base_formatter._fmt + ColouredFormatter.RESET, datefmt),
                logging.CRITICAL: logging.Formatter(ColouredFormatter.BOLD_RED + self.base_formatter._fmt + ColouredFormatter.RESET, datefmt)
            }

    def format(self, record):
        formatter = self.formatters.get(record.levelno) if self.coloured else self.base_formatter
        return formatter.format(record)